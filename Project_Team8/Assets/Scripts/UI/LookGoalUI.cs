﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookGoalUI : MonoBehaviour
{
    [SerializeField, Header("プレイヤー")]
    Transform player;
    [SerializeField,Header("ゴールの惑星")]
    Transform Goal;

    Vector3 dir;

    // Start is called before the first frame update
    void Start()
    {
        dir = Goal.position - player.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        dir = Goal.position - player.position;
        gameObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, dir);
    }
}
