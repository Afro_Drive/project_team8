﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0.1f;//基本となる暗転スピード
    public float fadeSpeed = 1.2f;//暗転スピード
    public float backSpeed = 1.0f;//明転スピード
    float alfa=0.0f;//デフォルトの暗転状態（まったく暗くないよ）
    float r, g, b;//rgb
    bool isPaint;//暗転可能かどうか
    void Start()
    {
        r = GetComponent<Image>().color.r;
        g = GetComponent<Image>().color.g;
        b = GetComponent<Image>().color.b;
        //デフォルトの色を固定。
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Image>().color = new Color(r, g, b, alfa);//色を変更可能にする
        if (isPaint == true)//暗転可能なら
        {
            FadeOut();//暗転する
        }
        if(isPaint==false)//暗転不可なら
        {
            FadeIn();//明転する
        }
    }
    public void FadeOut()//暗転メソッド
    {
        alfa += speed*fadeSpeed;//alfa値を加算する(徐々に暗くする
        if(alfa>=1)//alfa値が1以上（真っ暗）なら
        {
            PaintOff();//暗転不可にする
        }
    }
    public void FadeIn()//明転メソッド
    {
        if(alfa>=0f)//alfa値が0以上(色が残っている)なら
        alfa-=speed*backSpeed;//alfa値を減算する（徐々に赤ラルクする
    }
    public void PaintOn()//暗転可能にする
    {
        isPaint = true;
    }
    public void PaintOff()//暗転不可にする
    {
        isPaint = false;
    }
    public bool IsPaint()//暗転状態の確認
    {
        return isPaint;
    }

}
