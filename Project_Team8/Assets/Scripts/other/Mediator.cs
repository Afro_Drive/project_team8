﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 複数のゲームオブジェクト間のデータを伝達する仲介者
/// 作成者:谷 永吾
/// </summary>
public class Mediator : MonoBehaviour
{
    //フィールド
    GameObject player;
    PlayerController pContol;
    Transform planet;
    OrderZoomValue zoomSetter;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        pContol = player.GetComponent<PlayerController>();
        planet = null;
        zoomSetter = null;
        ZoomValue = 1;
    }

    // Update is called once per frame
    void Update()
    {
        //プレイヤーが公転状態になった
        if(pContol.GetStatus() == PlayerController.playerStatus.rotate)
        {
            //プレイヤーが公転している惑星を検索
            planet = player.transform.parent;
            //惑星ごとに設定されたズーム倍率を受け取る
            zoomSetter = planet.GetComponent<OrderZoomValue>();
            ZoomValue = zoomSetter.GetZoomValue();
        }
    }

    public int ZoomValue
    {
        get; private set;
    }

    public void SetNextScene(SceneName next)
    {
        SceneManagement.Load(next);
    }
}
