﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class ColorFader : MonoBehaviour
{
    // Start is called before the first frame update
    float alfa = 0.0f;//デフォルトの薄さ　最小＝0.0（何にも表示されない） 最大＝1.0(透過されない）
    float r, g, b;//rgb
    public float absolutealpha = 0.05f;//これ以上の薄さにはなんねーぞ！！って値
    void Start()
    {
        r = GetComponent<Image>().color.r;
        g = GetComponent<Image>().color.g;
        b = GetComponent<Image>().color.b;
        //デフォルトの色を固定。

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Image>().color = new Color(r, g, b, alfa);//色を変更可能にする
        
    }
    
   
    public void AlphaChange(float alpha)//薄さの変更　alpha(薄さの引数）
    {
        if(alpha<absolutealpha)//もし引数が規定値以下なら
        {
            alpha = absolutealpha;//引数を規定値に引き上げる
        }
        alfa = alpha;//引数を薄さに代入する
    }
}
