﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationFader : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0.1f;//基本となる暗転スピード
    public float fadeSpeed = 0.5f;//暗転スピード
    public float backSpeed = 0.5f;//明転スピード
    float alfa = 0.0f;//デフォルトの暗転状態（まったく暗くないよ）
    float r, g, b;//rgb
    bool isPaint;//暗転可能かどうか
    bool isClear;
    void Start()
    {
        r = GetComponent<SpriteRenderer>().color.r;
        g = GetComponent<SpriteRenderer>().color.g;
        b = GetComponent<SpriteRenderer>().color.b;
        //デフォルトの色を固定。
        ClearOff();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<SpriteRenderer>().color = new Color(r, g, b, alfa);//色を変更可能にする
        if (isPaint == true)//暗転可能なら
        {
            FadeOut();//暗転する
        }
        if (isClear == true)//暗転不可なら
        {
            FadeIn();//明転する
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (IsPaint())
                return;
            else
            PaintOn();
        }
        if(Input.GetKeyUp(KeyCode.Space) )
        {
            if (IsClear())
                return;
            else
            ClearOn();
        }
    }
    public void FadeOut()//暗転メソッド
    {
        alfa += speed * fadeSpeed;//alfa値を加算する(徐々に暗くする
        if (alfa >= 1)//alfa値が1以上（真っ暗）なら
        {
            PaintOff();//暗転不可にする
        }
    }
    public void FadeIn()//明転メソッド
    {
        alfa -= speed * backSpeed;//alfa値を減算する（徐々に明るくする
        if (alfa <= 0f)//alfa値が0以上(色が残っている)なら
        { 
            ClearOff();
        }
    }
    public void PaintOn()//暗転可能にする
    {
        isPaint = true;
        isClear = false;
        Debug.Log("PaintOn");
    }
    public void PaintOff()//暗転不可にする
    {
        isPaint = false;
        Debug.Log("PaintOff");
    }
    public void ClearOn()//明転可能にする
    {
        isClear = true;
        isPaint = false;
        Debug.Log("ClearOn");
    }
    public void ClearOff()//明転不可にする
    {
        isClear = false;
        Debug.Log("ClearOff");
    }
    public bool IsPaint()//暗転状態の確認
    {
        return isPaint;
    }
    public bool IsClear()//暗転状態の確認
    {
        return isClear;
    }

}
