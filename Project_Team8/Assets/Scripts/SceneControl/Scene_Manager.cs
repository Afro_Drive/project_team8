﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scene_Manager : MonoBehaviour
{
    
    [SerializeField, Header("フェード描写絵")]
    Image story;

    bool creditEnd;

    GameObject manager;
    SceneManagement smg;

    enum state
    {
        idle,
        credit,
        fadeIn,
        isStory,
        finish,
        fadeOut,
    }
    state currentState;

    // Start is called before the first frame update
    void Start()
    {
        story.color -= new Color(0, 0, 0, 1);
        GameObject manager = GameObject.Find("SceneManager");
        smg = manager.GetComponent<SceneManagement>();
        currentState = state.idle;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (currentState)
        {
            case state.idle:
                Idle();
                break;

            case state.credit:
                Credit();
                break;

            case state.fadeIn:
                FadeIn();
                break;

            case state.isStory:
                IsStory();
                break;

            case state.finish:
                Finish();
                break;

            case state.fadeOut:
                FadeOut();
                break;
        }
    }

    void Idle()
    {
       

        if (Input.anyKeyDown)
        {
            currentState = state.fadeIn;
        }
    }

    void Credit()
    {
        
            currentState = state.idle;

    }

    void FadeIn()
    {
        
            currentState = state.isStory;
    }

    void IsStory()
    {
        
            currentState = state.finish;
    }

    void Finish()
    {
        
            currentState = state.fadeOut;
        
    }

    void FadeOut()
    {
        story.color += new Color(0, 0, 0, 0.01f);
        if (story.color.a >=1f)
        {
            SceneManagement.Load(SceneName.Title);
        }
    }

}
