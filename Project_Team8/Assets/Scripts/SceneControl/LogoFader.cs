﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoFader : MonoBehaviour
{
    [SerializeField, Header("フェードスプライト")]
    private SpriteRenderer fade;

    enum State { In, Wait, Out, }
    private State state;
    private int logoCnt = 60;

    // Start is called before the first frame update
    void Start()
    {
        fade.color = new Color(0, 0, 0, 1);
        state = State.In;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateFade();
    }

    void UpdateFade()
    {
        switch (state)
        {
            case State.Out:
                FadeOut();
                break;
            case State.In:
                FadeIn();
                break;
            case State.Wait:
                StopFade();
                break;
        }
    }

    private void StopFade()
    {
        logoCnt--;
        if (logoCnt <= 0)
            state = State.Out;
    }

    private void FadeIn()
    {
        fade.color -= new Color(0, 0, 0, 0.008f);
        if (fade.color.a <= 0.0f)
            state = State.Wait;
    }

    private void FadeOut()
    {
        fade.color += new Color(0, 0, 0, 0.008f);
        if (fade.color.a >= 1.0f)
            SceneManagement.Load(SceneName.Title);
    }
}
