﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// チェックポイントのアニメーションを中心とした制御クラス
/// 作成者:谷 永吾
/// </summary>
public class FlagController : MonoBehaviour
{
    //フィールド
    Animator anim;

    // Start is called before the first frame update
    public void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetStandAnim()
    {
        anim.SetTrigger("Check");
    }
}
