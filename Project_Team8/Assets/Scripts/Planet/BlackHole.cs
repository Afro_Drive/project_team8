﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    GameObject player;

    PlayerKill pk;

    // Start is called before the first frame update
    void Start()
    {
        pk = GetComponentInParent<PlayerKill>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            player.transform.position += (transform.position - player.transform.position).normalized / 10;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        player = null;
    }
}
