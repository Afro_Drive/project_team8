﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDistance : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject GoalPlanet;//ゴールの惑星
    float FirstDistance;//初期スポーンとゴールとの距離
    float CurrentDistance;//現在位置とゴールとの距離
    float DistancePercentage;//それぞれの比率

    public GameObject colorFader;//薄さ変更メソッドを持ったUI

    void Start()
    {
        FirstDistance = Vector2.Distance(GoalPlanet.transform.position ,gameObject.transform.position);//初期距離の数値の固定
        //Debug.Log(FirstDistance);
    }

    // Update is called once per frame
    void Update()
    {
        CurrentDistance= Vector2.Distance(GoalPlanet.transform.position, gameObject.transform.position);//現在距離の数値を常に更新
        DistancePercentage = CurrentDistance / FirstDistance;//比率の更新
        colorFader.GetComponent<ColorFader>().AlphaChange(1-DistancePercentage);//薄さを変更するメソッドに比率の反比例を反映した引数を更新
       // Debug.Log(DistancePercentage);

       // Debug.Log(CurrentDistance);
    }
    public float VectorPercentage()
    {
        return DistancePercentage;
    }
}
