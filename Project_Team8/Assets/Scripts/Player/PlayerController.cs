﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //フィールド
    //旧PlayerMover関連
    private Vector2 getKeyDir, moveDir; //キー入力の方向、移動方向
    private float x, y;                 //キー入力のx方向y方向
    AudioSource audio;
    public AudioClip Jumpclip;
    public AudioClip Missclip;
    [SerializeField, Header("移動速度")]
    float speed = 1;
    [SerializeField, Header("移動方向補正　高いほど方向転換しづらくなります")]
    float directionCorrection = 150;
    
    //旧Rotation関連
    private GameObject parent;              //公転の中心となるオブジェクト
    private Vector2 distance;               //惑星とPlayerとをつなぐベクトル
    float angleR = 1f;                      //逆回転かどうかの補正値
    [SerializeField, Header("回転角度")]    //回転角度
    float nSpeed = 6.0f;   // オブジェクトの移動するスピード
    float nDegree = 0.0f;   // 度数値
    float nRadian;          // nDegreeの値をラジアンに変換した値
    float nRadiusX = 10.0f; // 楕円運動の水平方向の半径
    float dis = 0f;

    //PlayerContololler
    private Vector2 beforPos;   //前フレームのポジション

    public enum playerStatus   //Playerの状態
    {
        free,       //移動可能
        rotate,     //公転中
        freeze,     //操作不可　＊未使用
        blackHole,  //ブラックホール吸われ中
    }
    playerStatus status;        //現在の状態

    //リスポーン関連
    private Vector3 initialrespawn;
    private Vector3 currentrespawn;
    private int rLives = 5;             //残機

    private PlayerParticleManager pManager;     //パーティクルマネージャー

    [SerializeField, Header("最初に向かう惑星")]
    GameObject planet;

    public GameObject[] starIcons;
    public GameObject missFader;

    private Mediator mediator; //ゲーム仲介者

    private bool breakaway;
    void Start()
    {
        audio = GetComponent<AudioSource>();
        initialrespawn = transform.position;
        currentrespawn = initialrespawn;
        status = playerStatus.free; //Playerの状態を移動可能に
        moveDir = (planet.transform.position - transform.position).normalized;
        pManager = GetComponent<PlayerParticleManager>();
        UpdateStarIcons();

        mediator = GameObject.Find("GameMediator").GetComponent<Mediator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (status)
        {
            case playerStatus.free:
                Move();
                break;

            case playerStatus.rotate:
                Rotate();
                break;

            case playerStatus.blackHole:
                Rotate();
                break;

            case playerStatus.freeze:
                break;
        }
        
        beforPos = transform.position;
    }

    private void Update()
    {
        if (status != playerStatus.rotate)
            return;

        if (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.Space)) 
            breakaway = true;
    }

    /// <summary>
    /// 移動
    /// </summary>
    void Move()
    {
        //もし、現在の状態が移動可能でなければreturn
        if (status != playerStatus.free)
            return;

        //入力方向を取得、正規化してgetKeyDirに。
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        getKeyDir = new Vector2(x, y).normalized;

        //慣性をつけつつ移動
        moveDir = (moveDir + (getKeyDir / directionCorrection)).normalized;
        transform.position += (Vector3)(moveDir / 10) * speed;
    }

    /// <summary>
    /// 公転
    /// </summary>
    void Rotate()
    {
        //もし、現在の状態が公転中でなければreturn
        if (status != playerStatus.rotate && status != playerStatus.blackHole)
            return;

        //公転
        //transform.RotateAround(parent.transform.position, Vector3.forward, angle * angleR);
        //Set();

        //ここの処理はRotateXZとしてメソッド化すると見やすくなりそう
        nDegree = nDegree + nSpeed; // 移動する分の値をnDegreeに入れる
        nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する
        nRadian = nDegree * Mathf.Deg2Rad; // nDegreeの値をラジアンに変換した値をnRadianに入れる
        nRadian *= angleR;

        //XZ平面上を回転する
        transform.localPosition =
             new Vector3(
                 Mathf.Cos(nRadian + RotationdistanceAngle()) * dis,//nRadianが加算され座標が動的に変動していく
                 Mathf.Sin(nRadian + RotationdistanceAngle()) * dis,
                 0);
        
        moveDir = ((Vector2)transform.position - beforPos).normalized;

        if (status != playerStatus.rotate)
        {
            dis /= 1.02f;
            return;
        }
        //スペースキーが押されたら
        if (breakaway) 
        {
            audio.clip = Jumpclip;
            audio.Play();
            //親子関係解除
            transform.parent = null;
            //現在の状態を移動可能に
            status = playerStatus.free;
            //移動方向を直近の移動方向に
            moveDir = ((Vector2)transform.position - beforPos).normalized;
            pManager.BreakawayParticleStart(moveDir);
        }
    }

    /// <summary>
    /// 回転方向の設定
    /// </summary>
    void AngleSelect()
    {
        if ((CalcAngle() >= 0 && CalcAngle() < 180) || CalcAngle() < -180)
            angleR = 1;
        else
            angleR = -1;
        moveDir = Vector2.zero;
    }

    /// <summary>
    /// 移動方向の角度取得
    /// </summary>
    /// <returns></returns>
    float MoveAngle()
    {
        float mAngle = Mathf.Atan2(moveDir.y, moveDir.x) * Mathf.Rad2Deg;
        return mAngle;
    }

    /// <summary>
    /// 惑星との角度取得
    /// </summary>
    /// <returns></returns>
    float DistanceAngle()
    {
        //惑星と自身の座標の差分から、
        //X,Y座標で逆三角関数(タンジェント)を使用する
        //これにより惑星と自身の成す角度を算出する
        float dAngle = Mathf.Atan2(
           -distance.y, -distance.x)
            * Mathf.Rad2Deg;//弧度法で算出されたものを度数法にする
        return dAngle;
    }
    float RotationdistanceAngle()
    {
        float dAngle = Mathf.Atan2(
          distance.y, distance.x);//弧度法で算出されたものを度数法にする
        return dAngle;
    }
    /// <summary>
    /// 角度差取得
    /// </summary>
    /// <returns></returns>
    float CalcAngle()
    { return DistanceAngle() - MoveAngle(); }


    /// <summary>
    /// 公転角度の設定
    /// </summary>
    void Set()
    {
        nRadiusX = Mathf.Sqrt(((transform.position.x - parent.transform.position.x) * (transform.position.x - parent.transform.position.x)) +
           ((transform.position.y - parent.transform.position.y) * (transform.position.y - parent.transform.position.y)));
    }

    /// <summary>
    /// コリダーと当たったら
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter(Collider collision)
    {
        //もし、当たったのが惑星で、かつ、公転中でなければ
        if ((collision.tag == "Star" || collision.tag == "BlackHole") && collision.gameObject != parent)
        {
            breakaway = false;
            nDegree = 0;
            //公転の中心を惑星に
            parent = collision.gameObject;
            //惑星とPlayerの間のベクトルを設定
            distance = (transform.position - parent.transform.position).normalized;
            dis = collision.GetComponent<SphereCollider>().radius;
            //回転方向設定
            AngleSelect();
            if (collision.tag == "Star" && status != playerStatus.rotate)
                //状態を公転中に
                status = playerStatus.rotate;
            else if (collision.tag == "BlackHole" && status != playerStatus.blackHole) 
                status = playerStatus.blackHole;
            gameObject.transform.parent = collision.gameObject.transform;
        }
    }

    public playerStatus GetStatus()
    {
        return status;
    }

    public void Miss()//ミス判定スクリプト
    {
        audio.clip = Missclip;
        audio.Play();
        missFader.GetComponent<Fader>().PaintOn();//暗転スイッチオン
        Respawn();//復帰
    }
    public void RespawnSet(Vector3 res)
    {
        currentrespawn = res;
    }

    //条件達成時リスポーン
    public void Respawn()
    {
        if (rLives <= 0)//もし残機0で失敗したら
        {
            mediator.SetNextScene(SceneName.GameOver);
            Destroy(gameObject);//自身を破壊する
            return;
        }
        transform.position = currentrespawn;
        moveDir = (planet.transform.position - transform.position).normalized;
        rLives -= 1;
        status = playerStatus.free;
        transform.parent = null;
        parent = null;
        pManager.ParticleEnd();
        UpdateStarIcons();
    }

    public int RLive()
    {
        return rLives;
    }

    public void PlanetChange(GameObject planet)
    {
        this.planet = planet;
    }

    void UpdateStarIcons()
    {
        for(int i=0;i<starIcons.Length;i++)
        {
            if (RLive() > i)
                starIcons[i].SetActive(true);
            else
                starIcons[i].SetActive(false);
        }
    }

    public GameObject GetParent()
    {
        return parent;
    }

    public Vector3 GetMovedir()
    {
        return moveDir;
    }
    public float GetnSpeed()
    {
        return nSpeed;
    }
}
